import { inject, injectable } from 'inversify';
import { getVoiceConnection } from '@discordjs/voice';

import { Symbols } from '../../config/';
import { ConnectingChannelMap, GuildAudioPlayerMap } from './map/';
import { LeaveChannelCommand } from './';

@injectable()
export class LeaveChannelUseCase {
    constructor(
        @inject(Symbols.UseCase.Map.GuildAudioPlayerMap)
        private readonly guildAudioPlayerMap: GuildAudioPlayerMap,
        @inject(Symbols.UseCase.Map.ConnectingChannelMap)
        private readonly connectingChannelMap: ConnectingChannelMap
    ) {}

    readonly handle = (command: LeaveChannelCommand): void => {
        const conn = getVoiceConnection(command.guildId);
        conn?.disconnect();
        this.guildAudioPlayerMap.del(command.guildId);
        this.connectingChannelMap.del(command.guildId);
    }
}
