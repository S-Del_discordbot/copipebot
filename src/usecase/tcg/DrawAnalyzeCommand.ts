export interface DrawAnalyzeCommand {
    readonly deckSize: number;
    readonly drawCount: number;
    readonly targetCardCount: number;
}
